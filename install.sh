#!/bin/sh
# Copyright 2019 Urs Schulz
#
# This file is part of svgen.
#
# svgen is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# svgen is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with svgen.  If not, see <http://www.gnu.org/licenses/>.


if ! [ -d target ]; then
    echo "First run cargo build --release"
    exit 1
fi

install -m 0755 target/release/svgen /usr/local/bin
install -m 0755 templog /usr/local/bin
