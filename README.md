# svgen

[![crates.io](https://img.shields.io/crates/v/svgen.svg)](https://crates.io/crates/svgen)
[![docs.rs](https://docs.rs/svgen/badge.svg)](https://docs.rs/svgen/)

A small utility to generate runit services from templates.


## Usage
This tool looks for service templates in the directory `/etc/svgen/templates/` and instantiates them using instance descriptions in `/etc/svgen/instances`. A template is a directory containing at least a `run` file and optionally other files. Each line in the instances file must be of the form `<template-name>@<instance-name>`. Instantiation means that the files from the template are read, the string `__INSTANCE__` is replaced with the instance name and then a new service directory under `/etc/sv/generated/<instance-line>` is created. The processed files from the template directory are then written to the service directory while an existing `log` file is handled specially and installed as `log/run`. Afterwards the service is symlinked in `/service/` to activate it.


## Example
In this example we build a template for [[`wg-quick`]](https://www.wireguard.com/quickstart/), a simple program to setup wireguard VPN connections. This is a good example, since we usually have a bunch of VPN interfaces we want to set up and all the corresponding runit services would look nearly identical. Create a new directory `/etc/svgen/templates/wg-quick/` and put the following into the `run` file:

```shell
#!/bin/sh

wg-quick up "__INSTANCE__"
exec chpst -b "wg-quick@__INSTANCE__" pause
```

The `__INSTANCE__` string will get replaced upon instantiation of this template and takes the role of the interface name. In order to tear-down the network when the service is stopped, we also need a `finish` script:

```shell
#!/bin/sh

wg-quick down "__INSTANCE__"
```

Now, let's imagine you have configured two wireguard networks, `wg-net1` and `wg-net2`. In order to create services for them create the following `/etc/svgen/instances` file:

```
# wireguard
wg-quick@wg-net1
wg-quick@wg-net2
```

Now run
```bash
chmod +x run finish
svgen
```

This will generate two services under `/etc/sv/generated/` and symlink to them from `/service/`.


## Installation
To build the program, run
```bash
cargo build --release
```

and afterwards run as root:
```
bash
./install.sh
```


## Full operational description
See the [documentation](https://docs.rs/svgen/) for a full description on how this program operates.


## License
This project is licensed under the GNU AGPL version 3 or later, see the `LICENSE` file for more information.
